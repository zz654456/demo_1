/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React from 'react';
import {
  AppRegistry,
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  Button,
  Alert,
  TextInput,
  Image,
  StatusBar,
  NavigatorIOS,
} from 'react-native';

import {
  Header,
  LearnMoreLinks,
  Colors,
  DebugInstructions,
  ReloadInstructions,
} from 'react-native/Libraries/NewAppScreen';

import {
  accelerometer,
  gyroscope,
  setUpdateIntervalForType,
  SensorTypes
} from "react-native-sensors";

import Geolocation from '@react-native-community/geolocation';

import { map, filter } from "rxjs/operators";

// setUpdateIntervalForType(SensorTypes.accelerometer, 400); // defaults to 100ms

// const subscription = accelerometer
//   .pipe(map(({ x, y, z }) => x + y + z), filter(speed => speed > 20))
//   .subscribe(
//     speed => console.log(`You moved your phone with ${speed}`),
//     error => {
//       console.log("The sensor is not available");
//     }
//   );

//   setTimeout(() => {
//     // If it's the last subscription to accelerometer it will stop polling in the native API
//     subscription.unsubscribe();
//   }, 1000);

// const App: () => React$Node = () => {
//   return (
//     <>
//       <StatusBar barStyle="dark-content" /> 
//       <SafeAreaView>
//         <Text>
//           Welcome to React Native!
        
//           To get started, edit index.ios.js
        
//           Press Cmd+R to reload,{'\n'}
//           Cmd+Control+Z for dev menu  
//         </Text>
        
//       </SafeAreaView>
      
      
//     </>
//   ); 
// };

// Geolocation.getCurrentPosition(info => console.log(info));
// Geolocation.watchPosition(()=>Alert.alert('get'))

const App: () => React$Node = () => {
  return (
    <SafeAreaView>
      <View style={styles.container}>
        <View style={styles.titleView}>
          <View style={styles.viewOne}>
            <Text style={styles.title}>Cimee</Text>
          </View>
        </View>
        <View style={styles.midView}>
          <Text>middle</Text>
        </View>
        <View style={styles.btmView}>
          <Button style={styles.btn1}
            title="Press me"
            color="white"
            onPress={() => Alert.alert('Simple Button pressed')}
          />        
        </View>
      </View>
      
    </SafeAreaView>
    
  );
};

const styles = StyleSheet.create({
  container: {
    height: 800,
    // alignContent: 'stretch',
    // flex: 1,
    // backgroundColor: 'black',
    // flexDirection: 'column',
  },
  titleView: {
    
    justifyContent: 'center',
    backgroundColor: '#FFFF93',
    height: 100,
    marginTop: 40
  },
  title: {
    alignSelf: 'center',
    fontSize: 50,
    fontFamily: 'Cochin',
    color: 'black',
  },
  midView: {
    height: 400,
    backgroundColor: 'gray'
  },
  btmView: {
    height: 40,
    // justifyContent: 'flex-end',
    // alignItems: 'flex-end',
    // alignSelf: 'flex-end',
    // alignSelf: 'center',
    backgroundColor: '#84C1FF'
  },
})


export default App;
