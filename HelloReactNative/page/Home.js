
import React from 'react';
import { View, Text, Button } from 'react-native';


import Geolocation from '@react-native-community/geolocation'

let hasLocationPermission = true;
var data;
console.log('hiiii');
if (hasLocationPermission) {
  Geolocation.getCurrentPosition(
    (position) => {
      console.log(position);
      data = position
    },
    (error) => {
      // See error code charts below. 
      console.log(error.code, error.message); 
    },
    {
      enableHighAccuracy: true,
      timeout: 10000,
      maximumAge: 1
    }
   )
}


let greeting = 'Hey!';
let num = 87;

function HomeScreen({ navigation }) {
    return (
      <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center'}}>
        <Text>Home! Screen</Text>
        <Text>{greeting}</Text>
        <Button title="Push Home"
            onPress = { () => navigation.push('Home')}>
        </Button>
        <Button title="Go to Details"
            onPress={() => navigation.navigate('Details', {
                itemId: num,
                // otherParam: 'anything you want here'
                P: data
            })}>        
        </Button>
        <Button title="Contact" onPress={() => navigation.navigate('Contact')}></Button>
      </View>
    );
}

export default HomeScreen;