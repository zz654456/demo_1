import React from 'react';
import { View, Text, Button } from 'react-native';

function DetailsScreen({ route, navigation }) {

    const {itemId, otherParam, P} = route.params;

  return (
    <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center', flexWrap: 'wrap' }}>
      <Text>Details Screen</Text>
      <Text>itemId: {JSON.stringify(itemId)}</Text>
      {/* <Text>otherParam: {JSON.stringify(otherParam)}</Text> */}
      <Text>P: {JSON.stringify(P)}</Text>
      
      <Button
        title="Go to Details... again"
        onPress={() => navigation.push('Details')}
      />
      <Button title="Go to Home" onPress={() => navigation.navigate('Home')} />
      <Button title="Go back" onPress={() => navigation.goBack()} />
      <Button
        title="Go back to first screen in stack"
        onPress={() => navigation.popToTop()}
      />
    </View>
  );
}