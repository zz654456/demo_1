// In App.js in a new project

import * as React from 'react';
import { View, Text, Button } from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import { SafeAreaView } from 'react-native-safe-area-context';

function ContactScreen( { navigation }) {
  return (
    <SafeAreaView>
      <View>
        <Text>Contact Me!</Text>
      </View>
    </SafeAreaView>
    
  );
}

function HomeScreen({ navigation }) {
  return (
    <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
      <Text>Home! Screen</Text>
      <Text>{someData}</Text>
      <Button title="Go to Details" onPress={() => navigation.navigate('Details')}></Button>
      <Button title="Contact" onPress={() => navigation.navigate('Contact')}></Button>
    </View>
  );
}

// BASIC 
// function DetailsScreen({ navigation }) {
//   return (
//     <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
//       <Text>Details Screen</Text>
//       <Button
//         title="Go to Details... again"
//         onPress={() => navigation.push('Details')}
//       />
//     </View>
//   );
// }

// TRIGGER GO-BACK
function DetailsScreen({ navigation }) {
  return (
    <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
      <Text>Details Screen</Text>
      <Button
        title="Go to Details... again"
        onPress={() => navigation.push('Details')}
      />
      <Button title="Go to Home" onPress={() => navigation.navigate('Home')} />
      <Button title="Go back" onPress={() => navigation.goBack()} />
      <Button
        title="Go back to first screen in stack"
        onPress={() => navigation.popToTop()}
      />
    </View>
  );
}

const Stack = createStackNavigator();

var someData = 'hi';

function App() {
  return (
    <NavigationContainer>
      <Stack.Navigator initialRouteName="Home">
        <Stack.Screen name="Contact" component={ContactScreen} />
        <Stack.Screen name="Home">
          {props => <HomeScreen {...props} extraData={someData} />}
        </Stack.Screen>
        <Stack.Screen name="Details" component={DetailsScreen} />
      
      </Stack.Navigator>
    </NavigationContainer>
  );
}

export default App;