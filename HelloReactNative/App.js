// In App.js in a new project

import * as React from 'react';
import { View, Text, Button } from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import { SafeAreaView } from 'react-native-safe-area-context';


import HomeScreen from './page/Home';
// import DetailsScreen from './page/Details';

var testingword = 'hi';



function ContactScreen( { navigation }) {
  return (
    <SafeAreaView>
      <View>
        <Text>Contact Me!</Text>
      </View>
    </SafeAreaView>
    
  );
}

// TRIGGER GO-BACK
function DetailsScreen({ route, navigation }) {

    const {itemId, otherParam, P} = route.params;

  return (
    <View style={{ flex: 1, margin: 15 }}>
      <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center', backgroundColor: '#E6CAFF' }}>
        <Text>Details Screen</Text>
        <Text>itemId: {JSON.stringify(itemId)}</Text>
        {/* <Text>otherParam: {JSON.stringify(otherParam)}</Text> */}
        
        
        {/* <Button
          title="Go to Details... again"
          onPress={() => navigation.push('Details')}
        /> */}
        <Button title="Go to Home" onPress={() => navigation.navigate('Home')} />
        <Button title="Go back" onPress={() => navigation.goBack()} />
        <Button
          title="Go back to first screen in stack"
          onPress={() => navigation.popToTop()}
        />
      </View>
      
      <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center', backgroundColor: '#FF9797' }}>
        <Text style={{fontSize: 20}}>海拔 : {JSON.stringify(P.coords.altitude)}</Text>
        <Text style={{fontSize: 20}}>經度 : {JSON.stringify(P.coords.longitude)}</Text>
        <Text style={{fontSize: 20}}>緯度 : {JSON.stringify(P.coords.latitude)}</Text>
        <Text style={{fontSize: 20}}>速度 : {JSON.stringify(P.coords.speed)}</Text>
        <Text style={{fontSize: 20}}>More or Less : {JSON.stringify(P.coords.accuracy)} meters.</Text>
      </View>
    </View>
  );
}

const Stack = createStackNavigator();



function App() {
  return (
    <NavigationContainer>
      <Stack.Navigator initialRouteName="Home">
        <Stack.Screen name="Contact" component={ContactScreen} />
        <Stack.Screen name="Home">
          {props => <HomeScreen {...props} extraData={testingword} />}
        </Stack.Screen>
        <Stack.Screen name="Details" component={DetailsScreen} />
      
      </Stack.Navigator>
    </NavigationContainer>
  );
}

export default App;